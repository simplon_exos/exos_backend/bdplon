<?php

namespace App\DataFixtures;

use App\Entity\BD;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\Date;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder) {
        $this->encoder = $encoder;
    }
    public function load(ObjectManager $manager)
    {
        
        for ($i = 1; $i < 21; $i++) {
            $bd = new BD();
            $bd->setTitle('BD n° ' . $i)
                ->setAuthor('Jean de la ' . $i)
                ->setDescription('This book has been read ' . $i . ' times')
                ->setParution(new DateTime('now'));
            $manager->persist($bd);
        }
                
    for ($i = 1; $i < 5; $i++) {
        
        $user = new User();
        $hashedPassword = $this->encoder->encodePassword($user, '1234');
        $user->setRoles(['ROLE_USER'])
            ->setUsername('user' . $i)
            ->setPassword($hashedPassword);
        $manager->persist($user);
    }
    
    $admin = new User();
    $hashedPassword = $this->encoder->encodePassword($admin, 'admin');
    $admin->setRoles(['ROLE_ADMIN'])
            ->setUsername('admin')
            ->setPassword($hashedPassword);
    $manager->persist($admin);

    $manager->flush();

    }
}
