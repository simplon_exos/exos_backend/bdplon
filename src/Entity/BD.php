<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\File;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BDRepository")
 */
class BD
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $author;

    /**
     * @ORM\Column(type="text", length=455)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $parution;

    /**
     * @Assert\Image(maxSize="4M")
     */

    private $imgPath;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getParution(): ?\DateTimeInterface
    {
        return $this->parution;
    }

    public function setParution(?\DateTimeInterface $parution): self
    {
        $this->parution = $parution;

        return $this;
    }

    public function getImgPath(): ?File
    {
        return $this->imgPath;
    }

    public function setImgPath(?File $imgPath): self
    {
        $this->imgPath = $imgPath;

        return $this;
    }

}
