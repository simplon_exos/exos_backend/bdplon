<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Entity\User;
use App\Form\UserType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{

    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $utils)
    {
        $username = $utils->getLastUsername();
        $error = $utils->getLastAuthenticationError();

        // return $this->redirectToRoute('bd_index');

        return $this->render('user/login.html.twig', [
            'error' => $error,
            'username' => $username
        ]);
    }


    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
        throw new \Exception('ERROR !');
    }

    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //Assigner le rôle par défaut des user qui s'inscrivent
            $user->setRoles(['ROLE_USER']);
            //Hasher le mot de passe récupéré du formulaire avec l'encoder
            $hashedPassword = $encoder->encodePassword($user, $user->getPassword());
            //assigner le mot de passe hashé au user
            $user->setPassword($hashedPassword);

            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('/');
        }

        return $this->render('user/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
