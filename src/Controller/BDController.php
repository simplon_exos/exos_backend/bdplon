<?php

namespace App\Controller;

use App\Entity\BD;
use App\Form\BDType;
use App\Repository\BDRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use PHPUnit\Util\FileLoader;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\Routing\Annotation\Route;

class BDController extends AbstractController
{

    /**
     * @Route("/", name="bd_home")
     */
    public function home(BDRepository $bDRepository)
    {
        if (!$this->getUser()) {
            $repository = $this->getDoctrine()
                ->getManager()
                ->getRepository(BD::class);
            $listeBD = $repository->findBy(
                [],
                array('parution' => 'desc'),
                5,
                0
            );
        } else {
            $listeBD = $bDRepository->findAll();
        }
        return $this->render('/home.html.twig', [
            'bds' => $listeBD,
        ]);
    }

    /**
     * @Route("/bdIndex", name="bd_index")
     */
    public function index(BDRepository $bDRepository)
    {
        $bds = $bDRepository->findAll();

        return $this->render('bd/index.html.twig', [
            'bds' => $bds,
        ]);
    }

    /**
     * @Route("/user/addBD", name="add_BD")
     */
    // public function addBD(Request $request, ObjectManager $manager, FileLoader $loader)
    // {
    //     $bd = new BD();
    //     $form = $this->createForm(BDType::class, $bd);
    //     $form->handleRequest($request);

    //     if($form->isSubmitted() && $form->isValid()) {
    //         $imgPath = $loader->upload($bd->getImgPath());
    //         $bd->setImage($imgPath);

    //         $manager->persist($bd);
    //         $manager->flush();
    //         return $this->redirectToRoute('home');
    //     }

    //     return $this->render('user/addBD.html.twig', [
    //         'form' => $form->createView()
    //     ]);

    // }
}
