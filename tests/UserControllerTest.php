<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Process\Process;


class UserControllerTest extends WebTestCase

{
    /**
     * Méthode qui se déclenchera avant le début de chaque test de
     * la classe actuelle. Elle est utile pour mettre en place 
     * l'environnement de test, ici on l'utilise pour remettre à 
     * zéro la base de données en relancant la fixture
     */
    public function setUp()
    {
        $process = new Process(['php', 'bin/console', 'do:fi:lo']);
        $process->run();
    }

    public function testRegisterOk() // test US2 Scenario1
    {
        $client = static::createClient();
        $crawler = $client->request('GET', 'user/register');

        // On vérifie déjà que la page s'affiche bien
        $this->assertResponseIsSuccessful();

        //On récupère le formulaire en se basant sur le texte du button dans le template
        $form = $crawler->selectButton('Register')->form();

        //On remplit les différents champs du formulaire
        $form['user[username]'] = 'test@test.com';
        $form['user[password][first]'] = '1234';
        $form['user[password][second]'] = '1234';

        //On submit le formulaire
        $client->submit($form);

        $client->followRedirects();

        //On récupère le UserRepository "manuellement"
        $repo = static::$container->get('App\Repository\UserRepository');

        //On vérifie qu'un nouvel user a bien été ajouté dans la bdd
        $this->assertCount(6, $repo->findAll());
    }

    // public function testLogin() // testUs3
    // {
    //     $client = static::createClient();
    //     $crawler = $client->request('GET', '/user/login');

    //     $this->assertResponseIsSuccessful();

    //     $form = $crawler->selectButton('Sign In')->form([

    //         'form[_username]' => ('user1'),
    //         'form[_password]' => ('1234'),
    //     ]);
    //     $client->submit($form);
    // }
}
