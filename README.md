## USER STORIES
US2
Comme un visiteur,
Je veux pouvoir m'enregistrer sur le site,
Afin de pouvoir avoir accès au catalogue

=> Scénario1 US2
WHEN j'arrive sur la page d'accueil
THEN je clique sur le bouton Register
GIVEN un accès personnalisé.


US3
Comme un client,
je veux que l'on me propose différents types de bd,
Afin de parcourir rapidement différents styles et savoir ce que j’aime

=> Scénario US3
WHEN je rentre mon nom d'utilisateur et mon login,
AND que je clique sur le bouton 'Sign in',
THEN je suis redirigé vers la liste des bds,
GIVEN liste avec les détails de chaque bd.


US4
Comme un client, je veux pouvoir créer une liste de favoris de bd,
Afin de ne pas oublier de lire plus tard des BD que j’ai consulté

=> Scénario US4
WHEN arrive page d’accueil
THEN étoiles à cocher
GIVEN liste de bd cochés

US5 
Given : je suis un client qui vient de se connecter sur l’application BdPlon
et que j’arrive sur la page d’accueil qui présente l’ensemble des BD
When : je vois des filtres disponibles dans le haut de la page
et que je clique sur les filtres 
et que je sélectionne un type parmi les types de bd disponible
Then : une liste de bd correspondant au style sélectionné s’affiche
